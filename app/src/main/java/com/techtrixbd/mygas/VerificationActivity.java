package com.techtrixbd.mygas;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.util.Log;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.techtrixbd.mygas.model.ModelVerifyCode;
import com.techtrixbd.mygas.networking.APIService;
import com.techtrixbd.mygas.networking.ApiClient;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class VerificationActivity extends AppCompatActivity {
    TextView tvCodeSendSms, btnVerify;
    EditText etvCode;
    String vcode;
    APIService apiService;


    String codeSendMsg = "A verification code has been sent to ";
    String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        phoneNumber = getIntent().getStringExtra("NUMBER");

        init();
        //setValue();
        btnVerify.setOnClickListener(view -> {
            getValue();
            Verify(vcode);

        });

    }

    void Verify(String code) {

        apiService.verify(new ModelVerifyCode(code, phoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {

                            Gson gson = new Gson();
                            String json = gson.toJson(response);
                              Log.d("Datas: ",""+json);
                        },
                        t -> Log.d("test", "signUp: " + t+""));
    }

    void init() {
        tvCodeSendSms = findViewById(R.id.tv_activity_verification_msg);
        btnVerify = findViewById(R.id.btn_activity_verification_verify);
        etvCode = findViewById(R.id.et_activity_verification_code);
        apiService = ApiClient.getRetrofit();

        setValue();
    }

    void setValue() {
        tvCodeSendSms.setText(codeSendMsg + phoneNumber);
    }

    void getValue() {
        vcode = etvCode.getText().toString();
    }
}
