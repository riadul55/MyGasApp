package com.techtrixbd.mygas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.techtrixbd.mygas.R;
import com.techtrixbd.mygas.model.products.ResponseProducts.Product;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder>  {
    public interface ItemClickListener {
        void onClick(Product data);
    }

    private Context context;
    private List<Product> dataList;
    private ItemClickListener listener;

    public ProductListAdapter(Context context, List<Product> dataList, ItemClickListener listener) {
        this.context = context;
        this.dataList = dataList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListAdapter.ViewHolder holder, int i) {
        Product product = dataList.get(i);
        holder.bindData(product, i);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView item_cover;
        TextView item_name;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            item_cover = itemView.findViewById(R.id.item_cover);
            item_name = itemView.findViewById(R.id.item_name);
        }

        public void bindData(Product product, int i){

            Picasso.get().load(product.getImage())
                    .error(R.drawable.img_placeholder)
                    .placeholder(R.drawable.img_placeholder)
                    .fit().centerCrop().into(item_cover);

            item_name.setText(product.getName());
        }

        @Override
        public void onClick(View v) {
            listener.onClick(dataList.get(getAdapterPosition()));
        }
    }

    public void updateData(List<Product> dataList){
        this.dataList = dataList;
        notifyDataSetChanged();
    }
}
