package com.techtrixbd.mygas;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techtrixbd.mygas.model.ModelDistrictThana;
import com.techtrixbd.mygas.model.ModelSignUp;
import com.techtrixbd.mygas.networking.APIService;
import com.techtrixbd.mygas.networking.ApiClient;

import java.util.ArrayList;
import java.util.List;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class RegistrationActivity extends AppCompatActivity {
    private EditText etName, etPhoneNo, etPassword, etConfirmPassword;
    private TextView btnRegistration,btnAlreadyLogin;
    private Spinner spDistrict, spThana, spArea;
    APIService apiService1;
    ArrayAdapter adapter;
    String[] districtsString;
    String[] thanaString;
    String[] areaString;
    List<ModelDistrictThana.Datum> districtList;
    List<ModelDistrictThana.Datum> thanaList;
    List<ModelDistrictThana.Datum> areaList;
    private static final String TAG = "data";
    int areaId;
    String areaName;
    static boolean res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        hideToolbar();//hide android default toolbar
        init();//init all object
        getDistricts();

        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                getThana(districtList.get(i).getId().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spThana.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getArea(thanaList.get(i).getId().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                areaId = areaList.get(i).getId();
                areaName = areaList.get(i).getName();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btnRegistration.setOnClickListener(view -> signUp());
        btnAlreadyLogin.setOnClickListener(view -> finish());


    }

    private void init() {
        apiService1 = ApiClient.getRetrofit();
        etName = findViewById(R.id.et_activity_registration_name);
        etPhoneNo = findViewById(R.id.et_activity_registration_phone);
        etPassword = findViewById(R.id.et_activity_registration_password);
        etConfirmPassword = findViewById(R.id.et_activity_registration_confir_password);
        spDistrict = findViewById(R.id.sp_activity_registration_district);
        spThana = findViewById(R.id.sp_activity_registration_thana);
        spArea = findViewById(R.id.sp_activity_registration_area);
        btnRegistration = findViewById(R.id.btn_activity_registration_register);
        btnAlreadyLogin = findViewById(R.id.btn_activity_registration_login);

        districtsString = new String[100];
        districtList = new ArrayList<>();
    }

    void getArea(String id) {
        apiService1.getThana(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(modelThana -> {
                    areaList = modelThana.getData();
                    areaString = new String[modelThana.getData().size()];
                    for (int i = 0; i < modelThana.getData().size(); i++) {
                        areaString[i] = modelThana.getData().get(i).getName();
                    }

                    ArrayAdapter thanadapter = new ArrayAdapter(getApplicationContext(), R.layout.list_item, areaString);

                    spArea.setAdapter(thanadapter);

                }, t -> {
                });
    }

    void getThana(String id) {
        apiService1.getThana(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(modelThana -> {
                    thanaList = modelThana.getData();
                    thanaString = new String[modelThana.getData().size()];
                    for (int i = 0; i < modelThana.getData().size(); i++) {
                        thanaString[i] = modelThana.getData().get(i).getName();
                    }

                    ArrayAdapter thanadapter = new ArrayAdapter(getApplicationContext(), R.layout.list_item, thanaString);

                    spThana.setAdapter(thanadapter);

                }, t -> {
                });
    }

    void getDistricts() {
        apiService1.getDistrict()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(district -> {
                    districtList = district.getData();
                    for (int i = 0; i < 64; i++) {
                        districtsString[i] = district.getData().get(i).getName();
                    }

                    adapter = new ArrayAdapter(getApplicationContext(), R.layout.list_item, districtsString);

                    spDistrict.setAdapter(adapter);

                }, t -> {

                });
    }

    @SuppressLint("CheckResult")
    private void signUp() {
        //res = true;
        String name, mobile, password, confirmpassword, address;
        Double lat, lon;
        int area_id;
        name = etName.getText().toString();
        mobile = etPhoneNo.getText().toString();
        password = etPassword.getText().toString();
        confirmpassword = etConfirmPassword.getText().toString();
        area_id = areaId;
        address = areaName;
        lat = 43.0;
        lon = 90.0;
        //res = true;

        apiService1.signUp(new ModelSignUp(name, mobile, password, area_id, confirmpassword, address, lat, lon))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Log.d("code",response.getSuccess()+"");
                    if(response.getSuccess()){
                        Intent i = new Intent(getApplicationContext(),VerificationActivity.class);
                        i.putExtra("NUMBER",mobile);
                        startActivity(i);
                        finish();
                    }
                            try {
                                etName.setError(response.getData().getName().get(0) + "");
                            } catch (Exception e) {

                            }
                            try {
                                etPhoneNo.setError(response.getData().getMobileNo().get(0) + "");
                            } catch (Exception e) {

                            }
                            try {
                                etConfirmPassword.setError(response.getData().getPassword().get(0) + "");
                                etPassword.setError(response.getData().getPassword().get(0) + "");
                            } catch (Exception e) {

                            }



                            //Log.d("res",".."+response.getSuccess()+"");
                            /*if (response.getSuccess()!=false){
                                Log.d("res_s","success");

                            }else{
                                Log.d("res_else","false?");
                            }*/
                        },
                        t -> {


                    Log.d("MyError", "signUp: " + t.getMessage());
                        });

    }

    void hideToolbar() {
        try {
            getSupportActionBar().hide();
        } catch (Exception e) {

        }
    }


}
