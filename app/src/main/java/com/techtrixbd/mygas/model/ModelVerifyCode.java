package com.techtrixbd.mygas.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelVerifyCode implements Serializable
{

    @SerializedName("verification_code")
    @Expose
    private String verificationCode;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public ModelVerifyCode(String verificationCode, String mobileNo) {
        this.verificationCode = verificationCode;
        this.mobileNo = mobileNo;
    }

    public ModelVerifyCode(Boolean success, Data data, String message) {
        this.success = success;
        this.data = data;
        this.message = message;
    }



    public ModelVerifyCode() {
    }

    public class Data {

        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("user")
        @Expose
        private String user;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

    }

}