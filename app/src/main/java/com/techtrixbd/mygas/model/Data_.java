package com.techtrixbd.mygas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Data_ implements Serializable
{

    @SerializedName("name")
    @Expose
    private List<String> name = null;
    @SerializedName("mobile_no")
    @Expose
    private List<String> mobileNo = null;
    @SerializedName("password")
    @Expose
    private List<String> password = null;
    @SerializedName("area_id")
    @Expose
    private List<String> areaId = null;
    @SerializedName("address")
    @Expose
    private List<String> address = null;
    private final static long serialVersionUID = -3864582783813276898L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data_() {
    }

    /**
     *
     * @param password
     * @param areaId
     * @param address
     * @param name
     * @param mobileNo
     */
    public Data_(List<String> name, List<String> mobileNo, List<String> password, List<String> areaId, List<String> address) {
        super();
        this.name = name;
        this.mobileNo = mobileNo;
        this.password = password;
        this.areaId = areaId;
        this.address = address;
    }

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public List<String> getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(List<String> mobileNo) {
        this.mobileNo = mobileNo;
    }

    public List<String> getPassword() {
        return password;
    }

    public void setPassword(List<String> password) {
        this.password = password;
    }

    public List<String> getAreaId() {
        return areaId;
    }

    public void setAreaId(List<String> areaId) {
        this.areaId = areaId;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setAddress(List<String> address) {
        this.address = address;
    }

}
