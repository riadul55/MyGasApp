package com.techtrixbd.mygas.networking;
import com.techtrixbd.mygas.model.Data;
import com.techtrixbd.mygas.model.ModelDistrictThana;
import com.techtrixbd.mygas.model.ModelLogin;
import com.techtrixbd.mygas.model.ModelSignUp;
import com.techtrixbd.mygas.model.ModelVerifyCode;
import com.techtrixbd.mygas.model.products.ResponseProducts;


import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    @POST("api/v1/login")
    Observable<ModelLogin> logIn(@Body ModelLogin login);

    @POST("api/v1/register")
    Observable<Data> signUp(@Body ModelSignUp signKoro);

    @GET("api/v1/area/get_area")
    Observable<ModelDistrictThana> getDistrict();

    @GET("api/v1/area/get_child_area")
    Observable<ModelDistrictThana> getThana(@Query("parent_id") String id);

    @POST("api/v1/verify/mobile")
    Observable<ModelLogin> verify(@Body ModelVerifyCode code);


    @GET("api/v1/products/")
    Observable<ResponseProducts> getProductList();
}
