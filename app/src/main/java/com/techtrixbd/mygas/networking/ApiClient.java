package com.techtrixbd.mygas.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techtrixbd.mygas.helper.LoggingInterceptor;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static final String BASE_URL = "https://phplaravel-347681-1076043.cloudwaysapps.com/";
    private static Retrofit retrofit = null;

    private static APIService apiService;
    private static OkHttpClient httpClient = new OkHttpClient();

    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    public static APIService getRetrofit() {

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor())
                .build();


        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build();

            apiService = retrofit.create(APIService.class);
        }
        return apiService;
    }
}
