package com.techtrixbd.mygas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.techtrixbd.mygas.adapters.ProductListAdapter;
import com.techtrixbd.mygas.model.products.ResponseProducts.Product;
import com.techtrixbd.mygas.networking.APIService;
import com.techtrixbd.mygas.networking.ApiClient;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends BaseActivity implements ProductListAdapter.ItemClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private APIService apiService;

    RecyclerView recyclerList;
    ProductListAdapter mAdapter;
    List<Product> productList = new ArrayList<Product>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideToolbar();

        apiService = ApiClient.getRetrofit();

        initRecyclerList();

        getProductList();
    }

    private void getProductList() {
        apiService.getProductList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.getSuccess()){
                        mAdapter.updateData(response.getProductList());
                    } else {
                        showToast(response.getMessage());
                    }
                }, t -> {
                    Log.e(TAG, t.getMessage());
                });
    }

    private void initRecyclerList(){
        recyclerList = findViewById(R.id.recyclerList);
        GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
        recyclerList.setLayoutManager(layoutManager);
        mAdapter = new ProductListAdapter(context, productList, this);
        recyclerList.setAdapter(mAdapter);
    }

    void hideToolbar(){
        try {
            getSupportActionBar().hide();
        }catch (Exception e){

        }
    }


    @Override
    public void onClick(Product data) {
        showToast(data.getName());
    }
}
