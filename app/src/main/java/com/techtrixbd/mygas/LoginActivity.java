package com.techtrixbd.mygas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.techtrixbd.mygas.model.ModelLogin;
import com.techtrixbd.mygas.networking.APIService;
import com.techtrixbd.mygas.networking.ApiClient;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private APIService apiService;
    private EditText etPhone, etPassword;
    TextView btnLogin, btnSignUp;
    boolean resp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        hideToolbar();
        init();
        btnLogin.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);


    }

    void hideToolbar() {
        try {
            getSupportActionBar().hide();
        } catch (Exception e) {

        }
    }

    void init() {
        apiService = ApiClient.getRetrofit();
        etPhone = findViewById(R.id.et_activity_login_phone);
        etPassword = findViewById(R.id.et_activity_login_password);
        btnLogin = findViewById(R.id.btn_activity_login_login);
        btnSignUp = findViewById(R.id.btn_activity_login_sign_up);
    }

    void logIn(String phone, String pass) {
        //Call<ModelLogin> call = apiService.logIn(new ModelLogin("01511111111","01511111111"));

        apiService.logIn(new ModelLogin(phone, pass))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    resp = response.getSuccess();
                    if (resp == true) {
                        Toast.makeText(LoginActivity.this, "user verified....", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }else {
                        Toast.makeText(LoginActivity.this, "Phone No or Password is incorrect.", Toast.LENGTH_LONG).show();
                    }
                }, t -> Log.d("MyError", "" + t.getMessage()));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId() /*to get clicked view id**/) {
            case R.id.btn_activity_login_login:
                logIn(etPhone.getText().toString(), etPassword.getText().toString());
                break;
            case R.id.btn_activity_login_sign_up:
                Intent intent = new Intent(getApplicationContext(),RegistrationActivity.class);
                startActivity(intent);
                break;

        }
    }
}
